import pandas as pd
from discoverpy import Discover
discover = Discover()


def get_annotation_sources(endpoint):
    discover.set_dataset_repo(endpoint)
    annotation_dbs = discover.annotation_repo.get_annotation_databases()
    return annotation_dbs


def get_annotations(endpoint, data_lake, gene_list, sources):
    print(len(sources))
    print(type(sources))
    

    discover.set_dataset_repo(endpoint)
    annotations = list()
    # annotation_dbs = discover.annotation_repo.get_annotation_databases()
    if (type(sources) is str):
        db = sources
        df = discover.annotation_repo.get_feature_annotation(data_lake, db, gene_list)
        print(df)
        df = df.drop(columns=['_index', '_type', '_id', '_score', '_source.kw_index', '_source.kw_timestamp', '_source.kw_doc_id'])
        df.columns = [db+col if col != '_source.gene_id' else col for col in df.columns]
        annotations.append(df)
    else:
        for db in sources:
            df = discover.annotation_repo.get_feature_annotation(db, gene_list)
            print(df)
            df = df.drop(columns=['_index', '_type', '_id', '_score', '_source.kw_index', '_source.kw_timestamp', '_source.kw_doc_id'])
            df.columns = [db+col if col != '_source.gene_id' else col for col in df.columns]
            annotations.append(df)
      
    return annotations
