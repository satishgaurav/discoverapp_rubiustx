# import packages
from requests_aws4auth import AWS4Auth
import boto3
import requests
import json
from pandas.io.json import json_normalize
import pandas as pd



host = "https://lm4hm4en2f.execute-api.ap-southeast-1.amazonaws.com/pg/"
region = 'ap-southeast-1' # For example, us-west-1

def generate_dualcase_string(_str):
    _dual_str = []
    for _char in _str:
        if _char.isalpha():
            _dual_str.append(f"[{_char.lower()}{_char.upper()}]")
        else:
            _dual_str.append(_char)
    return "".join(_dual_str)


def query_dataset_by_field(field, value, endpoint, regexp = False, n = 10000):
        payload = { 
          "query": {
              "match": {field: value}
          },
        'size' : n
        }
        
        if regexp:
            payload  = {'query': {'regexp': 
                    {f'{field}.keyword':
                            {'value': f'.*{generate_dualcase_string(value)}.*',
                            'flags': 'ALL',
                            'max_determinized_states': 10000, 
                            'rewrite': 'constant_score'}}},
                            'size' : n
                            }
                            
        url =  host + endpoint + '/_search'
        r = requests.get(url, json = payload)
        if r.status_code != 200:
            r = requests.get(url, json = payload)
        if r.status_code != 200:
            r = requests.get(url, json = payload)
        if r.status_code != 200:
            return r.status_code
        output = r.json()
        search_df = json_normalize(output["hits"]["hits"])
        return search_df
        
def query_dataset_by_field_autocomplete(field, value, autofield,endpoint, regexp = False, n = 10000):
        payload = {
        "_source": ["organism",autofield],
          "query": {
              "match": {field: value}
          },
        'size' : n
        }
        url =  host + endpoint + '/_search'
        r = requests.get(url, json = payload)
        if r.status_code != 200:
            r = requests.get(url, json = payload)
        if r.status_code != 200:
            r = requests.get(url, json = payload)
        if r.status_code != 200:
            return r.status_code
        output = r.json()
        search_df = json_normalize(output["hits"]["hits"])
        return search_df
