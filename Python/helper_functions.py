import boto3
import logging
from botocore.exceptions import ClientError


def download_file(url):
    BUCKET_NAME = "discover-playground-datalake"
    KEY = "/".join(url.split("/")[3:])
    file_name = url.split("/")[-1]

    session = boto3.Session(aws_access_key_id = "AKIA4IUTUEUSJE4LJCYT", aws_secret_access_key = "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")
    s3 = session.resource('s3')

    try:
        s3.Bucket(BUCKET_NAME).download_file(KEY, file_name)
    except ClientError as e:
        if e.response['Error']['Code'] == "404":
            print("The object does not exist.")
        else:
            raise
    return file_name



def create_presigned_url(url, expiration=3600):
    """Generate a presigned URL to share an S3 object

    :param expiration: Time in seconds for the presigned URL to remain valid
    :return: Presigned URL as string. If error, returns None.
    """

    BUCKET_NAME = "discover-playground-datalake"
    KEY = "/".join(url.split("/")[3:])
    # Generate a presigned URL for the S3 object
    session = boto3.Session(aws_access_key_id = "AKIA4IUTUEUSJE4LJCYT", aws_secret_access_key = "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")
    s3_client = session.client('s3')

    try:
        response = s3_client.generate_presigned_url('get_object',
                                                    Params={'Bucket': BUCKET_NAME,
                                                            'Key': KEY},
                                                    ExpiresIn=expiration)
    except ClientError as e:
        logging.error(e)
        return None

    # The response contains the presigned URL
    return response