library(data.table)
library(dplyr)



curate_results <- function(result_list, algos_ran){
    results <- list()
    for (algo in algos_ran){
        if (algo == "diff_exp"){
            final_df_diff_exp <- do.call(rbind, result_list[["diff_exp"]])
            results[["diff_exp"]] <- final_df_diff_exp
            temp <- split(results[["diff_exp"]], results[["diff_exp"]]$name)
            v1 <- spread(temp$V1, key=rule, value=value)
            v2 <- spread(temp$V2, key=rule, value=value)
            condition_1 <- colnames(v1)[length(colnames(v1))]
            condition_2 <- colnames(v2)[length(colnames(v2))]
            v1 <- as.data.table(v1)[, toString(eval(parse(text = condition_1))), by = list(rowname)]
            names(v1)[names(v1) == "V1"] <- paste0(algo, "_", condition_1)
            v2 <- as.data.table(v2)[, toString(eval(parse(text = condition_2))), by = list(rowname)]
            names(v2)[names(v2) == "V1"] <- paste0(algo, "_", condition_2)
            results[["diff_exp"]] <- merge(v1, v2)
        }
        else if (algo == "x2k"){
            final_df_chea <- do.call(rbind, result_list[["chea"]])
            results[["chea"]] <- final_df_chea
            results[["chea"]] <- spread(results[["chea"]], key=rule, value=value)
            names(results[["chea"]])[names(results[["chea"]]) == "Name"] <- "rowname"
            condition_1 <- colnames(results[["chea"]])[length(colnames(results[["chea"]]))]
            results[["chea"]] <- as.data.table(results[["chea"]])[, toString(eval(parse(text = condition_1))), by = list(rowname)]
            names(results[["chea"]])[names(results[["chea"]]) == "V1"] <- paste0("chea_", condition_1)


            # for kinase enrichment analysis
            final_df_kea <- do.call(rbind, result_list[["kea"]])
            results[["kea"]] <- final_df_kea
            results[["kea"]] <- spread(results[["kea"]], key=rule, value=value)
            names(results[["kea"]])[names(results[["kea"]]) == "name.x"] <- "rowname"
            condition_2 <- colnames(results[["kea"]])[length(colnames(results[["kea"]]))]
            results[["kea"]] <- as.data.table(results[["kea"]])[, toString(eval(parse(text = condition_2))), by = list(rowname)]
            names(results[["kea"]])[names(results[["kea"]]) == "V1"] <- paste0("kea_", condition_2)

        }   
    }
    return(Reduce(function(dtf1, dtf2) merge(dtf1, dtf2, by = "rowname", all.x = TRUE, all.y = TRUE), results))
}