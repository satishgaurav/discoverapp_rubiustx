de2enricher <- function(diff_exp_rdesc, log2fc_range, p_val_cutoff, fdr_cutoff, gene_direction) {

  if(identical(fdr_cutoff, NULL)){
    diff_exp_rdesc <- diff_exp_rdesc[!is.infinite(rowSums(diff_exp_rdesc)),]
    x <- get_relevent_proteins(diff_exp_rdesc, p_val_cutoff, gene_direction, log2fc_range)
    abc <- rownames(x)
    s <- gsub("*.-.*", "", abc)
    s2 <- gsub(";", "\n", s)
    protein_selection_list <- unique(s2)
    return(protein_selection_list)
  } else{
    diff_exp_rdesc <- diff_exp_rdesc[!is.infinite(rowSums(diff_exp_rdesc)),]
    x <- get_relevent_proteins(diff_exp_rdesc, fdr_cutoff, gene_direction, log2fc_range)
    abc <- rownames(x)
    s <- gsub("*.-.*", "", abc)
    s2 <- gsub(";", "\n", s)
    protein_selection_list <- unique(s2)
    return(protein_selection_list)
  }
}
