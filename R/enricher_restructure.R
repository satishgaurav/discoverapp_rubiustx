
enricher_restructure <- function(enricher_df){
    enricher_df$P.value <- signif(as.numeric(as.character(unlist(enricher_df$P.value))), 2)
    enricher_df$Adjusted.P.value <- signif(as.numeric(as.character(unlist(enricher_df$Adjusted.P.value))), 2)
    enricher_df$Odds.Ratio <- round(as.numeric(as.character(unlist(enricher_df$Odds.Ratio))), 2)
    enricher_df$Combined.Score <- round(as.numeric(as.character(unlist(enricher_df$Combined.Score))), 2)

    rearranged_df <- cbind(enricher_df[, c(1:4)], enricher_df[, c(7:9)])
    return (rearranged_df)
}