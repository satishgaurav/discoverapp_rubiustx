library(fgsea)


fgsea_perform <- function(diff_exp, gmtfile, pvalcutoff, pvalcolumn
                         ,logFCcolumn, minSize, maxSize, nperm){
    
    print(head(diff_exp))
    pathways <- gmtPathways(gmtfile)
    diff_exp <- diff_exp[diff_exp[,pvalcolumn] < pvalcutoff,]
    diff_exp <- diff_exp[order(-diff_exp[logFCcolumn]),]
    ranks_test <- setNames(diff_exp[,logFCcolumn], row.names(diff_exp))
    fgseaRes <- fgsea(pathways, ranks_test, minSize = minSize, maxSize=maxSize, nperm=nperm)
}

