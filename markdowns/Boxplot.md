Boxplot can be really useful in understanding the distribution of expression within a dataset. For any downstream analysis such as differential expression or pathway analysis, the distribution has to be normal since they use tests which assume this distribution. An example of good boxplot is given below:

<p align="center">
 <img src="../images/boxplots-norm-1.png" alt="Boxplot"
    title="Discover Framework" width="500" height="450" /> 
</p>

A good boxplot has medians at the same level and a normal distribution. **Click on Make Boxplot! to see.**