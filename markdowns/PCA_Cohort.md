<a href="https://www.ncbi.nlm.nih.gov/geo/" target="_blank">GEO</a> datasets have metadata which is captured while a data is published on the repository. GEO has a template of adding metadata which everyone follows while submitting the data, while this is not curated, it is helpful to know which columns mean what. Below is the list of fields and their descriptions according to GEO template:

1. **title**: Unique title that describes the sample. The convention is [biomaterial] - [condition(s)] - [replicate number], e.g., Muscle_Exercised_60min_rep2. *Please don't expect it to be like this for every study*
2. **geo_accession**: Unique GEO id for each sample
3. **status**: Public or not
4. **channel_count**: How many channels present in the data, these mostly correspond to microarray data.
5. **source_name_ch1**: Biological material(s) and experimental variable(s), e.g., vastus lateralis muscle, exercised, 60 min
6. **organism_ch1**: Organism from which biological material was derived.
7. **characteristics_ch1**: All available characteristics of the biological material including factors not necessarily under investigation, e.g. gender, tissue, strain, developmental age, tumor stage etc. *characteristics_ch1.1, characteristics_ch1.2 ...* correspond to such characteristics column
8. **molecule_ch1**: Type of molecule that was extracted from biological material

Curation of the metadata is a huge problem for GEO datasets. We are making this better everyday.