Information about different datasets is stored as metadata in the datalake. The metadata consists of fields such as publication, source of the data etc.
The keyword gets searched in the following fields:

1. **Data Set Id**: Dataset ID consists of a recognizable id of data, for example, for GEO datasets it is the GEO accession of the data such as GSE37893.
2. **Data Set Source**: Source of the datasets, for example, GEO, TCGA or Pride
3. **Description**: This includes title of the publication or user provided description of the dataset.
4. **Diseases**: Disease to which dataset belongs.
5. **Organisms**: Organism for dataset, includes humans, mice and rat
6. **Platform**: Microarray or RNASeq
7. **Tissue**: Source tissue if the dataset such as blood, brain etc.