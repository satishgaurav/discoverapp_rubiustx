The custom pathway format follows similar format as <a href="https://software.broadinstitute.org/cancer/software/gsea/wiki/index.php/Data_formats#GMT:_Gene_Matrix_Transposed_file_format_.28.2A.gmt.29" target="_blank">gmt</a> of broad institute. It looks like this in a text editor.

```
Pathway	Proteins
Doherty et al. IFN responsive gene signature	IRF1,CXCL10,ISG15,IFI44
DNA damage response repair ISGs	LAMP3,LMO2,PARP10,PARP14,PARP9,SAMHD1
Type 1 IFN signature	IFIT1,IFI44,IFIT3,MX2
```
The upper part consist of two words: **Pathway**, **Proteins** separated by a tab and then a newline character. Subsequently you need to input pathway name and then a tab followed by genes separated by comma. You can do this in a text editor and save as a txt or csv file.