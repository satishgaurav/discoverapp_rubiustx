## Polly Discover App

Polly Discover is a ***platform for visualization, analytics and exploration of data sets***. It offers users curated data lakes from public repositories like GEO and apply a multitude of *state-of-the-art* analysis tools in order to explore and discover underlying biology.

Polly Discover App is a user friendly and interactive dashboard for analysis and visualization of transcriptomics data. Currently the platform handles gene expression microarray and RNA-seq data and support three species human, mouse and rat. A keyword based query can be made to search for datasets. The current capabilities include quality check using PCA, correlation of sample expression among metadata, pathway-specific gene expression, calculation of differential expression, global pathway analysis including X2K, Enrichr, GSEA and overlaying computations on KEGG pathways.

The workflow for the user can be divided into two separate steps, first is querying datasets from the data lake using keywords, second is selecting datasets based on the user's criteria and analyzing them for different aspects of the biological answer that is being sought.

## Architectural Overview

A high level overview of the underlying architecture can be seen below:

<p align="center">
 <img src="../images/Discover_Framework.png" alt="Discover Framework"
    title="Discover Framework" width="1000" height="450" /> 
</p>

Read more about data lakes <a href="https://docs.elucidata.io/Data%20Lake.html" target="_blank">here</a>.