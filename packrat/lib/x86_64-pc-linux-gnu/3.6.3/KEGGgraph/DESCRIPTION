Package: KEGGgraph
Type: Package
Title: KEGGgraph: A graph approach to KEGG PATHWAY in R and
    Bioconductor
Version: 1.46.0
Date: 2019-07-16
Author: Jitao David Zhang, with inputs from Paul Shannon
Maintainer: Jitao David Zhang <jitao_david.zhang@roche.com>
Description: KEGGGraph is an interface between KEGG pathway and graph
    object as well as a collection of tools to analyze, dissect and
    visualize these graphs. It parses the regularly updated KGML (KEGG
    XML) files into graph models maintaining all essential pathway
    attributes. The package offers functionalities including parsing,
    graph operation, visualization and etc.
License: GPL (>= 2)
LazyLoad: yes
Depends: R (>= 2.10.0)
Imports: methods, XML (>= 2.3-0), graph, utils, RCurl
Suggests: Rgraphviz, RBGL, testthat, RColorBrewer, KEGG.db,
    org.Hs.eg.db, hgu133plus2.db, SPIA
Collate: kegg2graph-functions.R parse.R annotation.R graph.R kgmlfile.R
    misc.R vis.R
URL: http://www.nextbiomotif.com
biocViews: Pathways, GraphAndNetwork, Visualization, KEGG
git_url: https://git.bioconductor.org/packages/KEGGgraph
git_branch: RELEASE_3_10
git_last_commit: 83fad70
git_last_commit_date: 2019-10-29
Date/Publication: 2019-10-29
NeedsCompilation: no
Packaged: 2019-10-29 23:50:32 UTC; biocbuild
Built: R 3.6.3; ; 2020-06-15 13:42:36 UTC; unix
InstallAgent: packrat 0.5.0
InstallSource: Bioconductor
Hash: a5643cca8682e0a77ebd254155dbc5b2
